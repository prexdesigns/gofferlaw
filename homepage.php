<?php
/* Template Name: Home Page Template */ 
get_header(); ?>

<main role="main">
	<!--Hero Section-->    
<section class="hero home">
    <div class="row">
    <div class="col col_2">
<div class="service-module home">
<img class="service-thumb" src="http://placehold.it/300x300" alt="" />
<div class="service-info">
<h2 class="secondary-heading">Service Name</h2>
<p>YOU'RE the Chiclet! Not me. Caw ca caw, caw ca caw, caw ca caw! A night of heterosexual intercourse. I should be in this Poof. Michael was concerned that he was caught in a lie about his family.
</p>
</div>
</div>

<div class="service-module home clear">
<img class="service-thumb" src="http://placehold.it/300x300" alt="" />
<div class="service-info">
<h2 class="secondary-heading">Service Name</h2>
<p>YOU'RE the Chiclet! Not me. Caw ca caw, caw ca caw, caw ca caw! A night of heterosexual intercourse. I should be in this Poof. Michael was concerned that he was caught in a lie about his family.
</p>
</div>
</div>

<div class="service-module home clear">
<img class="service-thumb" src="http://placehold.it/300x300" alt="" />
<div class="service-info">
<h2 class="secondary-heading">Service Name</h2>
<p>YOU'RE the Chiclet! Not me. Caw ca caw, caw ca caw, caw ca caw! A night of heterosexual intercourse. I should be in this Poof. Michael was concerned that he was caught in a lie about his family.
</p>
</div>
</div>

<div class="service-module home clear">
<img class="service-thumb" src="http://placehold.it/300x300" alt="" />
<div class="service-info">
<h2 class="secondary-heading">Service Name</h2>
<p>YOU'RE the Chiclet! Not me. Caw ca caw, caw ca caw, caw ca caw! A night of heterosexual intercourse. I should be in this Poof. Michael was concerned that he was caught in a lie about his family.
</p>
</div>
</div>
    
    </div>
    <div class="col col_2">
    <div class="what-is">
    <h2 class="alpha"><b>What Is A Board Certified Trial Attorney?</b></h2>
        <p>Certified Civil Trial Lawyer Requirements For Certification:</p>
<ul>
	<li>Practice of law for at least 5 years</li>
	<li>Substantial involvement in conducting trials and settlements</li>
	<li>Substantial participation in continuing legal education</li>
	<li>Reputation among judges and other lawyers for competence</li>
	<li>Successful completion of written examination</li>
</ul>
                <div class="home-video"><iframe src="https://www.youtube.com/embed/O8Nnn06Um0I" frameborder="0" allowfullscreen></iframe></div>

    </div>
    </div>

    </div>    
         
     </section>
        

    <!--/ hero -->

    <!-- Settlements Won-->
    <section class="settlements">
    <div class="row">

    <div role="banner" class="settle-slider">
    <div class="settle">
    <blockquote>
    <p>Bacon ipsum dolor amet shankle short loin pancetta pork pig tongue. Ground round shoulder bresaola capicola landjaeger tail picanha porchetta beef ham hock pancetta ribeye venison boudin flank. Turkey turducken strip steak, chuck pork belly boudin short ribs pork chop tongue. Sausage short ribs ball tip jowl corned beef t-bone.
</p>
    <cite>
    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg"/>
    <strong>Some Guy</strong></cite>
    </blockquote>
        </div>

            <div class="settle">

      <blockquote>
    <p>Bacon ipsum dolor amet shankle short loin pancetta pork pig tongue. Ground round shoulder bresaola capicola landjaeger tail picanha porchetta beef ham hock pancetta ribeye venison boudin flank. Turkey turducken strip steak, chuck pork belly boudin short ribs pork chop tongue. Sausage short ribs ball tip jowl corned beef t-bone.
</p>
    <cite>
    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/kurafire/128.jpg"/>
    <strong>Some Other Guy</strong></cite>
    </blockquote>
        </div>


            <div class="settle">

      <blockquote>
    <p>Bacon ipsum dolor amet shankle short loin pancetta pork pig tongue. Ground round shoulder bresaola capicola landjaeger tail picanha porchetta beef ham hock pancetta ribeye venison boudin flank. Turkey turducken strip steak, chuck pork belly boudin short ribs pork chop tongue. Sausage short ribs ball tip jowl corned beef t-bone.
</p>
    <cite>
    <img src="https://s3.amazonaws.com/uifaces/faces/twitter/ok/128.jpg"/>
    <strong>Some Other Guy</strong></cite>
    </blockquote>
        </div>



    </div>

    </div>

    </section>


    <!--News and Social-->
    <section class="news">
    <div class="row">
        <section class="col col_2 articles-home" role="contentinfo">
            <?php query_posts('category_name=blog&posts_per_page=3');?>
            <?php while(have_posts()) : the_post();?>
            <h2><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h2>
            <p><?php the_excerpt('Read More...'); ?></p>
            <?php endwhile; ?>



        </section>


        <div class="col col_2"></div>

        </div>

    </section>


	</main>


<?php get_footer(); ?>
