<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <!--Plugins-->
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.3/slick.css"/>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.3/slick-theme.css"/>



		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>

        	<!-- header -->
			<header class="header clear" role="banner">
                    <div class="row">
                        <div class="col col_2"><h2 class="logo">Logo</h2></div>
				
                        <div class="col col_2 ulitity" role="contentinfo">
                        <h4>Proudly Serving NEPA</h4>
                        <a href="tel:570-342-3207">570-342-3207</a>

                        <!-- social -->
                        <nav class="header-social">
                        </nav>
                        <!-- /social -->
                        </div>

                          <!-- Mobile Toggle -->
            <div class="mobile-toggle"><span></span></div>
            <!-- /Mobile Toggle -->
                </div>
                <div class="row">
					<!-- nav -->
					<div class="nav col col_2 offset" role="navigation">
                        <nav class="header-main-nav"><?php html5blank_nav(4); ?></nav>
					</div>
					<!-- /nav -->


                </div>

			</header>
			<!-- /header -->
        
		<!-- wrapper -->
		<div class="wrapper">

		
